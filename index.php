<?php

require_once('animal.php');
require_once('another.php');
$sheep = new animal("kambing");

echo "jenis hewan : " . $sheep->name . "<br>";
echo "jumlah kaki : " . $sheep->legs . "<br>";
echo "berdarah dingin : " . $sheep->cold_blood . "<br> <br>";

$apes = new ape("monyet");
echo "jenis hewan : " . $apes->name . "<br>";
echo "jumlah kaki : " . $apes->legs . "<br>";
echo "berdarah dingin : " . $apes->cold_blood . "<br>";
echo  $apes->yell() . "<br> <br>";

$frogs = new frog("katak");
echo "jenis hewan : " . $frogs->name . "<br>";
echo "jumlah kaki : " . $frogs->legs . "<br>";
echo "berdarah dingin : " . $frogs->cold_blood . "<br>";
echo  $frogs->jump() . "<br> <br>";

?>